package ru.rks.tasks.task7;
import java.util.Scanner;

/**
 * Created by USer on 26.01.2017.
 */
public class task7 {
    static Scanner scanner = new Scanner(System.in);
    public static void main (String[]args){
        int num = (int)(Math.random()*10);
        testing(num);
    }
    public static void testing (int num){
        int n=0;
        do {
            System.out.println("Введите число");
            n  = scanner.nextInt();
            if(n==num){
                System.out.println("Угадал");
            }
            if(n>num){
                System.out.println("Загаданное число меньше");
            }
            if(n<num){
                System.out.println("Загаданное число больше");
            }
        } while (n!=num);
    }
}

package ru.rks.tasks.task14;

import java.util.Scanner;

/**
 * Created by USer on 31.01.2017.
 */
public class task14 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите  символ");
        String text = scanner.next();
        verification(text);
    }

    /**
     *Метод проверяет , является ли введенный символ числом , буквой или знаком пунктуации
     *
     * @param text
     */
    public static void verification (String text) {
        char symbol = text.charAt(0);
        if (Character.isDigit(symbol)) System.out.println("Симовл является цифрой");
        if (Character.isLetter(symbol)) System.out.println("Симол является буквой");
        if (".,?!;:".contains(text)) System.out.println("Символ является знаком пунктуации");
    }
}


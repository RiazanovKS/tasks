package ru.rks.tasks.task12;

import java.util.Scanner;

/**
 * Created by USer on 27.01.2017.
 */
public class task12 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int days = 0;
        do {
            System.out.println("Введите кол-во дней ");
            days = scanner.nextInt();
        } while (days <= 0);
        System.out.println("Кол-во часов - " + days * 24 + " Кол-во минут - " + days * 1440 + " Кол-во секунд - " + days * 86400);
    }
}

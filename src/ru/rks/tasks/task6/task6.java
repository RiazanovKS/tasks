package ru.rks.tasks.task6;
import java.util.Scanner;

/**
 * Created by USer on 26.01.2017.
 */
public class task6 {
    static  Scanner scanner = new Scanner(System.in);
    public static void main (String []args){
        System.out.println("Введите число");
        int n = scanner.nextInt();
        table(n);
    }
    public static void table (int num){
        System.out.println("Крайнее число в диапазоне умножения - ");
        int diapason = scanner.nextInt();
        for(int i =1;i<=diapason;i++){
            System.out.println(num + " * "+i+ " = "+(num*i) );
        }
    }
}

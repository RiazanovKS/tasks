package ru.rks.tasks.task1;
import java.util.Scanner;

/**
 * Created by USer on 31.01.2017.
 */
public class task1 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String text = scanner.nextLine();
        System.out.println("Кол-во элементов перед точкой = " + text.indexOf("."));
        System.out.println("Кол-во пробелов перед точкой = " + amountOfSpaces(text));

    }

    public static int amountOfSpaces(String text) {
        int amountOfSpaces = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                amountOfSpaces++;
            }
        }
        return amountOfSpaces;

    }
}
